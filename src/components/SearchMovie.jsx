import React, { Component } from "react";
import "./styles.css";

class SearchMovie extends Component {
  state = { searchValue: "",
movies:[]
};

  handleOnChange = (event) => {
    this.setState({ searchValue: event.target.value });
  };
  handleSearch = () => {
}

  render() {
    return (
      <div>
        <input
          className="searchbar"
          type="text"
          placeholder="What do you want to watch?"
          name="search"
          onChange={(event) => this.handleOnChange(event)}
          value={this.state.searchValue}
        ></input>
        <button className="searchbutton" onClick={this.handleSearch}>
          SEARCH
        </button>
      </div>
    );
  }
}

export default SearchMovie;
