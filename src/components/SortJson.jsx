import React, { useState, useEffect } from 'react';
// import './App.css';

const bands = [
  // {
  //   id: 1,
  //   name: 'Nightwish',
  //   albums: 9,
  //   members: 6,
  //   formed_in: 1996,
  // },
  // {
  //   id: 2,
  //   name: 'Metallica',
  //   albums: 10,
  //   members: 4,
  //   formed_in: 1981,
  // },
  // {
  //   id: 3,
  //   name: 'Nirvana',
  //   albums: 3,
  //   members: 3,
  //   formed_in: 1987,
  // },
  {
    Title: "Star Wars: Episode IV - A New Hope",
    Year: "1977",
    imdbID: "tt0076759",
    Type: "Comedy",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 4.3,
  },
  {
    Title: "Star Wars: Episode V - The Empire Strikes Back",
    Year: "1980",
    imdbID: "tt0080684",
    Type: "Horror",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 4.5,
  },
  {
    Title: "Star Wars: Episode VI - Return of the Jedi",
    Year: "1983",
    imdbID: "tt0086190",
    Type: "Drama",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BOWZlMjFiYzgtMTUzNC00Y2IzLTk1NTMtZmNhMTczNTk0ODk1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 3.8,
  },
  {
    Title: "Star Wars: Episode VII - The Force Awakens",
    Year: "2015",
    imdbID: "tt2488496",
    Type: "Drama",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 4.1,
  },
];

function SortJson() {
  const [data, setData] = useState([]);
  const [sortType, setSortType] = useState('albums');

  useEffect(() => {
    const sortArray = type => {
      const types = {
        albums: 'albums',
        members: 'members',
        formed: 'formed_in',
      };
      const sortProperty = types[type];
      const sorted = [...bands].sort((a, b) => b[sortProperty] - a[sortProperty]);
      setData(sorted);
    };

    sortArray(sortType);
  }, [sortType]); 

  return (
    <div className="App">
      <select onChange={(e) => setSortType(e.target.value)}> 
        <option value="albums">Albums</option>
        <option value="members">Members</option>
        <option value="formed">Formed in</option>
      </select>

      {data.map(band => (
        <div key={band.id} style={{ margin: '30px' }}>
          <div>{`Band: ${band.name}`}</div>
          <div>{`Albums: ${band.albums}`}</div>
          <div>{`Members: ${band.members}`}</div>
          <div>{`Year of founding: ${band.formed_in}`}</div>
        </div>
      ))}
    </div>
  );
}

export default SortJson;