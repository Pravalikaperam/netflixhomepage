import React, { Component } from "react";
import { useState, useEffect } from "react";
import "./styles.css";
import SearchMovie from "./SearchMovie";
import { Link, NavLink } from "react-router-dom";

const movies = [
  {
    Title: "Star Wars: Episode IV - A New Hope",
    Year: "1977",
    imdbID: "tt0076759",
    Type: "Comedy",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 4.3,
  },
  {
    Title: "Star Wars: Episode V - The Empire Strikes Back",
    Year: "1980",
    imdbID: "tt0080684",
    Type: "Horror",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 4.5,
  },
  {
    Title: "Star Wars: Episode VI - Return of the Jedi",
    Year: "1983",
    imdbID: "tt0086190",
    Type: "Drama",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BOWZlMjFiYzgtMTUzNC00Y2IzLTk1NTMtZmNhMTczNTk0ODk1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 3.8,
  },
  {
    Title: "Star Wars: Episode VII - The Force Awakens",
    Year: "2015",
    imdbID: "tt2488496",
    Type: "Drama",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX300.jpg",
    Description:
      "The Phantom Menace was released on May 19, 1999, and Episode II: Attack of the Clones on May 16, 2002. Episode III: Revenge of the Sith, the first PG-13 film in the franchise, was released on May 19, 2005.[67] The first two movies were met with mixed reviews, with the third being received somewhat more positively. The trilogy begins 32 years before Episode IV and follows the Jedi training of Anakin Skywalker, Luke's father, and his eventual fall from grace and transformation into the Sith lord Darth Vader, as well as the corruption of the Galactic Republic and rise of the Empire led by Darth Sidious. Together with the original trilogy, Lucas has collectively referred to the first six episodic films of the franchise as the tragedy of Darth Vader.",
    Rating: 4.1,
  },
];

// class searchbar extends Component {
function SearchBar() {
  const [data, setData] = useState([]);
  const [sortType, sortMovies] = useState("Year");

  useEffect(() => {
    const sortArray = (type) => {
      const types = {
        Rating: "Rating",
        Year: "Year",
        Type: "Type",
      };
      const sortProperty = types[type];
      const sorted = [...movies].sort(
        (a, b) => b[sortProperty] - a[sortProperty]
      );
      setData(sorted);
    };

    sortArray(sortType);
  }, [sortType]);

  // render() {
  console.log("given array", movies);
  // const netflix = JSON.stringify(movies);
  // console.log("given json", netflix);
  return (
    <div>
      <div className="container1">
        <p className="para">
          <b className="para">netflix</b>roulette
        </p>
        <button className="addbutton">+ ADD MOVIE</button>
        {/* <Link className="addbutton" to="/movie/add">+ADD MOVIE</Link> */}
        <h1 className="find"> FIND YOUR MOVIE</h1>
        {/* <div>
          <input
            className="searchbar"
            type="text"
            placeholder="What do you want to watch?"
            name="search" onChange={event => this.handleOnChange(event)}
            value={this.state.searchValue}
          ></input>
          <button className="searchbutton">SEARCH</button>
        </div> */}
        <SearchMovie />
      </div>
      <div className="container2">
        <div className="topnav">
          <a className="active" href="#home">
            ALL
          </a>
          <a href="#news">DOCUMENTARY</a>
          <a href="#contact">COMEDY</a>
          <a href="#about">HORROR</a>
          <a href="#about">CRIME</a>

          <div className="dropdown">
            <label className="dropbtnsort">SORT BY</label>
            <select
              className="dropbtn"
              onChange={(e) => sortMovies(e.target.value)}
            >
              <option value="Release">RELEASE DATE</option>
              <option value="Rating">RATING</option>
              <option value="Type">TYPE</option>
              <option value="Year">YEAR</option>
            </select>
          </div>
        </div>
        <p className="found">39 MOVIES FOUND</p>
<div className="movie">
        {data.map((movie) => (
          // <div className="grid">
            // <div className="card">
              <div key={movie.id} style={{ margin: "30px" }}>
                <img width="270" src={movie.Poster} />
                <div>{`Title: ${movie.Title}`}</div>
                <div>{`Year: ${movie.Year}`}</div>
                <div>{`Type: ${movie.Type}`}</div>
                <div>{`Rating: ${movie.Rating}`}</div>
              </div>
            // </div>
          // </div>
        ))}</div>
        <p className="para2">
          <b className="para2">netflix </b>roulette
        </p>
      </div>
    </div>
  );
}
export default SearchBar;
